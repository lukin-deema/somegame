/**
 * Created by Lukin on 27.09.2016.
 */
import React from "react";
import {withRouter} from "react-router";

class Footer extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="footer">Footer block</div>
        );
    }
}
export default withRouter(Footer);