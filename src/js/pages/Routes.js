/**
 * Created by Lukin on 27.09.2016.
 */
import React from "react";
import MainPage from "./MainPage";

export const getAppRoutes = () => {
    const appRoutes = {
        path: '/',
        indexRoute: {component: MainPage},
    };
    return appRoutes;
};
