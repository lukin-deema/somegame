/**
 * Created by Lukin on 27.09.2016.
 */
import React from "react";
import {withRouter} from "react-router";
import dispatcher from "../store/Dispatcher";
import { CASE, EVENT } from "../common/constants";

class Header extends React.Component{

    constructor(props) {
        super(props);
    }
    click(){
        dispatcher.dispatch({type:CASE.BASIC, event: EVENT.TOGGLE_LEFT})
    }
    render() {
        return (
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Brand</a>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a onClick={this.click.bind(this)}>Link</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default withRouter(Header);