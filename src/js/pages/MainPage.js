/**
 * Created by Lukin on 27.09.2016.
 */
import React from "react";
import {withRouter} from "react-router";
import Header from './Header';
import Footer from './Footer';
import Store from "../store/Store";
import { CASE, EVENT } from "../common/constants";

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            toggle:true,
        };
        this.toggle = this.toggle.bind(this);
    }
    componentWillMount(){
        Store.on(EVENT.TOGGLE_LEFT, this.toggle);
    }
    componentWillUnmount(){
        Store.removeListener(EVENT.TOGGLE_LEFT, this.toggle);
    }
    toggle(){
        this.setState({
            toggle: !this.state.toggle,
        })
    }
    render() {
        return (
        <div className="col-md-offset-3 col-md-6">
            <Header/>
            TODO {this.state.toggle? "MainPage":"toggle"}
            <Footer/>
        </div>
        );
    }
}
export default withRouter(MainPage);
