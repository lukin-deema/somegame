/**
 * Created by Lukin on 27.09.2016.
 */
import React from "react";
import ReactDOM from "react-dom";
import App from "./pages/App";
import "../styles/app.css";

const app = document.getElementById('app');
ReactDOM.render(<App/>, app);
