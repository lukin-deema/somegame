/**
 * Created by Lukin on 27.09.2016.
 */
import { EventEmitter } from "events";
import dispatcher from "./Dispatcher";
import { CASE } from "../common/constants";

class eventStore extends EventEmitter {
    constructor() {
        super();
    }

    handleActions(action) {
        switch (action.type) {
            case CASE.BASIC:{
                if(action.event){
                    this.emit(action.event);
                } else {
                    console.error(`event does not exist`);
                }
                break;
            }
        }
    }
}

const Store = new eventStore;
dispatcher.register(Store.handleActions.bind(Store));

export default Store;
